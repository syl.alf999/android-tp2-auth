package com.sylvie.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AuthenticationActivity extends AppCompatActivity{
    Button btnAuthenticate;
    EditText etLogin, etPassword;
    TextView tvResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        //Get UI components
        btnAuthenticate = findViewById(R.id.btnAuthenticate);
        etLogin = findViewById(R.id.etLogin);
        etPassword = findViewById(R.id.etPassword);
        tvResult = findViewById(R.id.tvResult);

        tvResult.setVisibility(View.GONE);
    }

    /**
     * Button handling the authentication with an HttpURLConnection object
     * @param view
     */
    public void btn_authenticate(View view){

        //String urlString = "https://www.android.com/";
        String urlString = "https://httpbin.org/basic-auth/bob/sympa";

        String login = etLogin.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if(login.isEmpty() || password.isEmpty()) {
            Toast.makeText(AuthenticationActivity.this, R.string.Fill_required_fields, Toast.LENGTH_SHORT).show();
            return;
        }

        Thread thread = new Thread(() -> {
            URL url = null;
            try{
                url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                String basicAuth = "Basic " + Base64.encodeToString((login + ":" + password).getBytes(), Base64.NO_WRAP);
                urlConnection.setRequestProperty ("Authorization", basicAuth);

                try{
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String s = readStream(in);
                    Log.i("JFL", s);

                    JSONObject json = new JSONObject(s);
                    String res = json.getString("authenticated"); //get the value of authenticated

                    //Update UI
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // We refresh the result TextView...
                            tvResult.setText("My result here !\n" + res);
                            tvResult.setVisibility(View.VISIBLE);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                } finally{
                    urlConnection.disconnect();
                }
            }
            catch(MalformedURLException e) {
                e.printStackTrace();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        });

        thread.start();
    }


    /**
     * Read the header response from @param is
     * This function was defined by another user
     * @param is
     * @return
     * @throws IOException
     */
    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

}